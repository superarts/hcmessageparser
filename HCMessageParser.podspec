#
# Be sure to run `pod lib lint HCMessageParser.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "HCMessageParser"
  s.version          = "0.1.0"
  s.summary          = "A message parser."

  s.description      = <<-DESC
`HCMessageParser` parses `message` and returns a dictionary with mentions, emoticons, and links within it. If `blockDone` is passed, asynchronized call(s) will be executed and the titles of all the web links will be filled in the result dictionary and sent to `blockDone` then all the web links are processed. If there are any errors, a dictionary with URL strings as keys and `NSError`s as values will be passed to the block as well.
                       DESC

  s.homepage         = "https://bitbucket.org/superarts/hcmessageparser"
  s.license          = 'MIT'
  s.author           = { "Leo" => "leo@superarts.org" }
  s.source           = { :git => "https://superarts@bitbucket.org/superarts/hcmessageparser.git", :tag => s.version.to_s }
  s.social_media_url = 'https://linkedin.com/in/superarts'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'HCMessageParser' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end