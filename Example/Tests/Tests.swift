// https://github.com/Quick/Quick

import Quick
import Nimble
import HCMessageParser

class TableOfContentsSpec: QuickSpec {
    override func spec() {
		describe("mention") {
			let key = HCMessageParser.kMention
			it("can have letters and numeric characters") {
				for mention in ["leo", "ChristX", "Bob123"] {
					//expect(HCMessageParser.mention("hi @\(mention)")?[0]) == mention
					expect(HCMessageParser.parse("hi @\(mention)")[key]?[0] as? String) == mention
				}
			}
			it("can have multiple usernames and perserve order") {
				let mentions = ["bob", "john", "doe"]
				let message = "@\(mentions[0]) @\(mentions[1])@\(mentions[2]) (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
				for (index, mention) in mentions.enumerate() {
					expect(HCMessageParser.parse(message)[key]?[index] as? String) == mention
				}
			}
			it("cannot have . - _ etc.") {
				expect(HCMessageParser.parse("where is @john.123?")[key]?[0] as? String) == "john"
				expect(HCMessageParser.parse("can't find you guys @.@ where are you")[key]).to(beNil())
				expect(HCMessageParser.parse("I'm lost @_@")[key]).to(beNil())
				expect(HCMessageParser.parse("@-@ HELP!!")[key]).to(beNil())
				expect(HCMessageParser.parse("@,@ @man, you there? @,@")[key]?[0] as? String) == "man"
			}
			it("can be 1 to 49 characters long") {
				let username49 = "1234567891234567891234567891234567890000123456789"
				let username50 = "12345678912345678912345678912345678900001234567890"
				expect(HCMessageParser.parse("hi @L")[key]?[0] as? String) == "L"
				expect(HCMessageParser.parse("It can be @" + username49)[key]?[0] as? String) == username49
				expect(HCMessageParser.parse("It cannot be @" + username50)[key]).to(beNil())
				expect(HCMessageParser.parse("It cannot be! @@")[key]).to(beNil())
			}
			it("can have extended latin characters") {
				for mention in ["über", "André"] {
					expect(HCMessageParser.parse("happy birthday @\(mention)!")[key]?[0] as? String) == mention
				}
			}
			it("can have CJK characters") {
				expect(HCMessageParser.parse("我是@刘日天")[key]?[0] as? String) == "刘日天"
				expect(HCMessageParser.parse("私は @レオ です")[key]?[0] as? String) == "レオ"
				expect(HCMessageParser.parse("나는 @레오 입니다")[key]?[0] as? String) == "레오"
			}
			it("can identify CJK punctuation") {
				expect(HCMessageParser.parse("@老板，你在哪？")[key]?[0] as? String) == "老板"
			}
			it("can count CJK characters correctly") {
				let username49 = "一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九"
				let username50 = "一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十"
				expect(HCMessageParser.parse("It can be @" + username49)[key]?[0] as? String) == username49
				expect(HCMessageParser.parse("It cannot be @" + username50)[key]).to(beNil())
			}
			it("cannot have emoji CJK characters") {
				expect(HCMessageParser.parse("I'm so @😀 lol")[key]).to(beNil())
			}
		}

		describe("emoticon") {
			let key = HCMessageParser.kEmoticon
			it("can be alphanumeric strings") {
				let message = "Good morning! (megusta) (coffee)"
				expect(HCMessageParser.parse(message)[key]?[0] as? String) == "megusta"
				expect(HCMessageParser.parse(message)[key]?[1] as? String) == "coffee"
				expect(HCMessageParser.parse("I like (sunshine1)")[key]?[0] as? String) == "sunshine1"
			}
			it("can be converted to lowercase automatically by default") {
				expect(HCMessageParser.parse("I (LOL)ed")[key]?[0] as? String) == "lol"
			}
			it("can toggle lowercase converting using flag HCMessageParser.emoticonForceLowercase") {
				expect(HCMessageParser.parse("Happy birthday (cAkE)")[key]?[0] as? String) == "cake"
				HCMessageParser.emoticonForceLowercase = false
				expect(HCMessageParser.parse("Happy birthday (cAkE)")[key]?[0] as? String) == "cAkE"
				HCMessageParser.emoticonForceLowercase = true
				expect(HCMessageParser.parse("Happy birthday (cAkE)")[key]?[0] as? String) == "cake"
			}
			it("can be up to 15 characters long") {
				let emoticon15 = "123456789012345"
				let emoticon16 = "1234567890123456"
				expect(HCMessageParser.parse("want some (" + emoticon15 + ")?")[key]?[0] as? String) == emoticon15
				expect(HCMessageParser.parse("not some (" + emoticon16 + ")!")[key]).to(beNil())
			}
		}

		describe("URL") {
			let key = HCMessageParser.kLink
			it("can be http(s) by default") {
				let urlTail = "twitter.com/jdorfman/status/430511497475670016?q1=a1&q2=a2"
				for url in ["http://" + urlTail, "https://" + urlTail] {
					expect(HCMessageParser.parse("try \(url)!")[key]?[0][HCMessageParser.kURL]) == url
				}
			}
			it("can be naked URLs by default") {
				for url in ["www.test.com", "example.org"] {
					expect(HCMessageParser.parse("try \(url)...")[key]?[0][HCMessageParser.kURL]) == url
				}
			}
			it("can support multi-languages") {
				expect(HCMessageParser.parse("try http://www.test.com/测试!")[key]?[0][HCMessageParser.kURL]) == "http://www.test.com/测试"
				expect(HCMessageParser.parse("try http://موقع.وزارة-الاتصالات.مصر/!")[key]?[0][HCMessageParser.kURL]) == "http://موقع.وزارة-الاتصالات.مصر/"
			}
			it("can add support of other protocols") {
				expect(HCMessageParser.parse("file transfer protocol ftp://myfiles.com/user1")[key]?[0][HCMessageParser.kURL]) != "ftp://myfiles.com/user1"

				//	Add supported protocols
				HCMessageParser.urlSupportedProtocols.append("ftp")
				expect(HCMessageParser.parse("file transfer protocol ftp://myfiles.com/user1")[key]?[0][HCMessageParser.kURL]) == "ftp://myfiles.com/user1"

				//	Reset supported protocols
				HCMessageParser.urlSupportedProtocols = HCMessageParser.urlDefaultProtocols
				expect(HCMessageParser.parse("file transfer protocol ftp://myfiles.com/user1")[key]?[0][HCMessageParser.kURL]) != "ftp://myfiles.com/user1"
			}
			it("can get title") {
				waitUntil(timeout: 20) { done in
					let links = HCMessageParser.parse("getting title from naked URL: www.google.com") {
						(links, errors) in
						expect(links?[key]?[0][HCMessageParser.kURL]) == "www.google.com"
						expect(links?[key]?[0][HCMessageParser.kTitle]) == "Google"
						done()
					}[key]
					expect(links?[0][HCMessageParser.kURL]) == "www.google.com"
				}
			}
			it("can get title and decode HTML entities") {
				waitUntil(timeout: 20) { done in
					let links = HCMessageParser.parse("Is https://twitter.com/jdorfman/status/430511497475670016 readable?") {
						(links, errors) in
						expect(links?[key]?[0][HCMessageParser.kURL]) == "https://twitter.com/jdorfman/status/430511497475670016"
						expect(links?[key]?[0][HCMessageParser.kTitle]) == "Justin Dorfman on Twitter: nice @littlebigdetai..."
						done()
					}[key]
					expect(links?[0][HCMessageParser.kURL]) == "https://twitter.com/jdorfman/status/430511497475670016"
				}
			}
		}
    }
}