public typealias HCBlockParseDone	= ((result: [String: AnyObject]?, errors: [String: NSError]?) -> Void)
public typealias HCBlockURLDone		= ((links: [[String: String]]?, errors: [String: NSError]?) -> Void)

/**
    Parses `message` and returns a dictionary with mentions, emoticons, and links within it. Check `HCMessageParser.parse` for details.
*/
public class HCMessageParser {
	/**
        Force emoticons to be converted to lowercase strings, is `true` as default.
	*/
	public static var emoticonForceLowercase = true

	/**
        Default protocols that include `http`, `https`, and naked URLs like example.com
	*/
	public static let urlDefaultProtocols = ["https?"]
	/**
        Array with supported protocols for URL parsing, is `[urlDefaultProtocols]` as default.
	*/
	public static var urlSupportedProtocols = urlDefaultProtocols

	/**
        Const key for `mention`
	*/
	public static let kMention		= "mentions"
	/**
        Const key for `emoticon`
	*/
	public static let kEmoticon		= "emoticons"
	/**
        Const key for `link`
	*/
	public static let kLink			= "links"
	/**
        Const key for `url`
	*/
	public static let kURL			= "url"
	/**
        Const key for `title`
	*/
	public static let kTitle		= "title"

	/**
        Parses `message` and returns a dictionary with mentions, emoticons, and links within it. If `blockDone` is passed, asynchronized call(s) will be executed and the titles of all the web links will be filled in the result dictionary and sent to `blockDone` then all the web links are processed. If there are any errors, a dictionary with URL strings as keys and `NSError`s as values will be passed to the block as well.

        Example:
		
		Test message: 
		"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016 but http://i.amnothing.com is down (frown)"

		Returns: 
		["links": (
				{
				url = "https://twitter.com/jdorfman/status/430511497475670016";
			},
				{
				url = "http://i.amnothing.com";
			}
		), "mentions": (
			bob,
			john
		), "emoticons": (
			success,
			frown
		)]

		Callback with result dictionary: 
		["links": (
				{
				title = "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\"";
				url = "https://twitter.com/jdorfman/status/430511497475670016";
			},
				{
				url = "http://i.amnothing.com";
			}
		), "mentions": (
			bob,
			john
		), "emoticons": (
			success,
			frown
		)]

		And error dictionary:
		["http://i.amnothing.com": Error Domain=NSURLErrorDomain Code=-1003 "A server with the specified hostname could not be found." UserInfo={NSUnderlyingError=0x7ff5b95324a0 {Error Domain=kCFErrorDomainCFNetwork Code=-1003 "(null)" UserInfo={_kCFStreamErrorCodeKey=8, _kCFStreamErrorDomainKey=12}}, NSErrorFailingURLStringKey=http://i.amnothing.com/, NSErrorFailingURLKey=http://i.amnothing.com/, _kCFStreamErrorDomainKey=12, _kCFStreamErrorCodeKey=8, NSLocalizedDescription=A server with the specified hostname could not be found.}]

        - Parameters:
            - message: The message to be parsed
            - blockDone: The block gets called when all titles are obtained, is `nil` as default

        - Returns: The dictionary that contains parsed informations, including metions, emoticons, and links
    */
	public class func parse(message: String, blockDone: HCBlockParseDone? = nil) -> [String: AnyObject] {
		var dict = [String: AnyObject]()
		if let mentions = mention(message) {
			dict[kMention] = mentions
		}
		if let emoticons = emoticon(message) {
			dict[kEmoticon] = emoticons
		}
		let blockURLDone: HCBlockURLDone = {
			(results, errors) in
			dict[kLink] = results
			if let block = blockDone {
				block(result: dict, errors: errors)
			}
		}
		if let links = url(message, blockDone: blockURLDone) {
			dict[kLink] = links
		}
		return dict
	}

	private static let mentionMax = 49
	private static let mentionPattern = String(format: "@([\\p{L}0-9]{1,%i})", mentionMax + 1)

	/**
        Captures mentioned names from message.

        - Parameters:
            - message: The message to be parsed

        - Returns: The array that contains all the captured strings. If nothing is captured, `nil` is returned
    */
	private class func mention(message: String) -> [String]? {
		return match(message, pattern: mentionPattern, locationFix: 1, lengthFix: -1, max: mentionMax)
	}

	private static let emoticonMax = 15
	private static let emoticonPattern = String(format: "\\([A-Za-z0-9]{1,%i}\\)", emoticonMax + 1)

	/**
        Captures emoticons from message.

        - Parameters:
            - message: The message to be parsed

        - Returns: The array that contains all the captured strings. If nothing is captured, `nil` is returned
    */
	private class func emoticon(message: String) -> [String]? {
		return match(message, pattern: emoticonPattern, locationFix: 1, lengthFix: -2, max: emoticonMax, forceLowercase: emoticonForceLowercase)
	}

	private static let urlPatternHead = "(?i)\\b((?:"
	private static let urlPatternTail = ":" +
			"(?:/{1,3}|[a-z0-9%])|[a-z0-9.\\-]+[.]" +
			"(?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)" +
		"/)(" + 
			"?:[^\\s()<>{}\\[\\]]+|\\([^\\s()]*?\\([^\\s()]+\\)[^\\s()]*?\\)|\\([^\\s]+?\\))+(?:\\([^\\s()]*?\\([^\\s()]+\\)[^\\s()]*?\\)|" +
			"\\([^\\s]+?\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’])|(?:(?<!@)[a-z0-9]+(?:[.\\-][a-z0-9]+)*[.]" + 
			"(?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)" + 
		"\\b/?(?!@)))"
	private static let urlPatternHTMLTitle = "<title>(.*?)</title>"
	private static var urlPattern: String {
		return urlPatternHead + urlSupportedProtocols.joinWithSeparator("|") + urlPatternTail
	}

	/**
        Default blacklisted characters, see `README` for details.
	*/
	private static var urlDefaultTitleFilters = ["\""]
	/**
        Array with blacklisted characters for URL titles, is `[urlDefaultTitleFilters]` as default.
	*/
	private static var urlTitleFilters = urlDefaultTitleFilters
	/**
        URL title length limit, see `README` for details. To remove, set it to nil. It has to be longer than the length of ellipsis.
	*/
	private static var urlTitleMax: Int? = 50
	private static let urlTitleEllipsis = "..."

	/**
        Captures URLs from message.

        - Parameters:
            - message: The message to be parsed
            - blockDone: The block gets called when all titles are obtained, is `nil` as default

        - Returns: The array that contains dictionaries with `url` and `title` as keys, and URLs and titles as values
    */
	private class func url(message: String, blockDone: HCBlockURLDone? = nil) -> [[String: String]]? {
		if let links = match(message, pattern: urlPattern) {
			var array = [[String: String]]()
			var httpLinks = [String]()
			var index = 0
			var dictTitle = [String: AnyObject]()
			for link in links {
				array.append([kURL: link])

				//	Title is only available for http(s) URLs (including naked URLs like example.com)
				var httpLink = link
				if link.rangeOfString(":") == nil {
					httpLink = "http://" + link
				}

				//	Store index of links so that titles can be filled into array later
				dictTitle[httpLink] = index
				index++

				if httpLink.hasPrefix("http") {
					httpLinks.append(httpLink)
				}
			}

			//	Get titles for httpLinks when callback block is provided
			if let block = blockDone {
				var dictError = [String: NSError]()
				var count = 0
				for link in httpLinks {

					let session = NSURLSession.sharedSession()
					if let url = NSURL(string: link) {
						let request = NSURLRequest(URL: url)
						let task = session.dataTaskWithRequest(request, completionHandler: {
							(data, response, error) in   

							//	Get encoding to support UTF8, ISO 8859-1, GBK, etc.
							var usedEncoding = NSUTF8StringEncoding
							if let encodingName = response?.textEncodingName {
								let encoding = CFStringConvertEncodingToNSStringEncoding(CFStringConvertIANACharSetNameToEncoding(encodingName))
								if encoding != UInt(kCFStringEncodingInvalidId) {
									usedEncoding = encoding
								}
							}
							if let data = data, let html = String(data: data, encoding: usedEncoding) {
								if let titles = match(html, pattern: urlPatternHTMLTitle, locationFix: 7, lengthFix: -15) where titles.count > 0 {
									var title = titles[0]

									//	Decode HTML entities
									let encodedData = title.dataUsingEncoding(NSUTF8StringEncoding)!
									let attributedOptions: [String: AnyObject] = [
										NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
										NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding,
									]
									do {
										let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
										title = attributedString.string

										//	Remove blacklisted characters
										for filter in urlTitleFilters {
											title = title.stringByReplacingOccurrencesOfString(filter, withString:"")
										}
										//	Truncate title
										if let max = urlTitleMax where title.characters.count > max && max > urlTitleEllipsis.characters.count {
											let length = max - urlTitleEllipsis.characters.count
											title = (title as NSString).substringToIndex(length) + urlTitleEllipsis
										}

										if let i = dictTitle[link] as? Int {
											array[i][kTitle] = title
										}
									} catch let error as NSError {
										dictError[link] = error
									}
								} else {
									//	print("no title found, doesn't count as an error")
								}
							} else if let error = error {
								dictError[link] = error
							} else {
								//	print("data decoding failed, don't report error")
							}
							count++

							//	Callback when all titles are obtained
							if count == httpLinks.count {
								if dictError.count == 0 {
									block(links: array, errors: nil)
								} else {
									block(links: array, errors: dictError)
								}
							}
						})
						task.resume()
					}
				}
			}
			return array
		}
		return nil
	}

	/**
        Captures strings `message` using `pattern`.

        - Parameters:
            - message: The message to be parsed
            - pattern: Regex pattern for `NSRegularExpression`
            - locationFix: Offset of range location, e.g. `@xxx` it should be 1
            - lengthFix: Offset of range length, e.g. for `(xxx)` it should be 2
            - max: length limit, is `nil` as default which means no limit
            - forceLowercase: convert return strings to lowercase, is `false` as default

        - Returns: The array that contains all the captured strings. If nothing is captured, `nil` is returned
    */
	private class func match(message: String, pattern: String, locationFix: Int = 0, lengthFix: Int = 0, max: Int? = nil, forceLowercase: Bool = false) -> [String]? {
		do {
			let regex = try NSRegularExpression(pattern: pattern, options: [])
			let all = NSRange(location: 0, length: message.characters.count)
			var matches = [String]()
			regex.enumerateMatchesInString(message, options: [], range: all) {
				(result, flags, stop) in
				if var range = result?.range {
					range.location += locationFix
					range.length += lengthFix
					var str = (message as NSString).substringWithRange(range) as String
					if let max = max where str.characters.count > max {
						return
					}
					if forceLowercase {
						str = str.lowercaseString
					}
					matches.append(str)
				}
			}
			if matches.count > 0 {
				return matches
			}
		} catch _ {
			//	print("regex error")
		}
		return nil
	}
}